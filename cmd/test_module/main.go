package main

import (
	"github.com/gin-gonic/gin"
	"github.com/natefinch/pie"
	"log"
	"net/http"
	"net/rpc"
)

func main() {
	p := plug{pie.NewConsumer()}
	s, err := p.Handle("GET", "/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})
	if err != nil {
		log.Fatalf("failed saying hi: %s", err)
	}
	log.Println("Got response from host: ", s)
}

type plug struct {
	*rpc.Client
}

func (p plug) Handle(httpMethod, relativePath string, handlers ...gin.HandlerFunc) (result string, err error) {
	err = p.Call("router.Handle", []interface{}{httpMethod, relativePath, handlers}, &result)
	return result, err
}
