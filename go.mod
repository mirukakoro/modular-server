module gitlab.com/colourdelete/modular-server

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/natefinch/pie v0.0.0-20170715172608-9a0d72014007
)
