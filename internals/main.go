package internals

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/colourdelete/modular-server/pkg/modules/manager"
	"os"
)

type rootNamesStruct struct {
	router *gin.Router
}

func initServer() {
	router := gin.Default()
	rootNames := rootNamesStruct{
		router: router,
	}
	server, err := pie.StartConsumer(os.Stderr, path)
	if err != nil {
		panic(err)
	}
	testModule := manager.Module{
		config: manager.ModuleConfig{
			URI:         "gitlab.com/colourdelete/modular/server/cmd/test_module",
			Name:        "Test Module",
			Author:      "Ken Shibata (@colourdelete on GitLab.com)",
			VersionStr:  "v0.0.0-alpha",
			VersionHash: "",
			Methods: []string{
				"/router",
			},
		},
		path:   "../test_module.exe",
		server: server,
		loaded: false, // optional, but don't set to true
	}
	router.Run()
}
